#!/usr/bin/env python3

__author__ = "Paul Zeinlinger"
__version__ = "0.1.0"

import argparse
from logzero import logger
import math
import time
import matplotlib

matplotlib.use("AGG")


class MyRandomGenerator:
    def __init__(self, seed, modulus=2 ** 48, a=25214903917, c=11):
        self.__seed = seed
        self.__modulus = modulus
        self.__a = a
        self.__c = c

    def rand(self):
        self.__seed = (self.__a * self.__seed + self.__c) % self.__modulus
        return self.__seed

    def uni01(self):
        return self.rand() / self.__modulus

    def uni(self, imax):
        return int(self.uni01() * imax)


def f(x):
    return math.exp(x)


analytic_result = math.exp(1) - 1


def main(args):
    import matplotlib.pyplot as plt

    """ Main entry point of the app """
    logger.info("Monte-Carlo Integral, Version: {}".format(__version__))
    steps = 1000000
    x_min = 0
    x_max = 1
    y_min = f(0)
    y_max = y_min
    dx = x_max - x_min
    for i in range(steps):
        x = x_min + dx * float(i) / steps
        y = f(x)
        if y < y_min:
            y_min = y
        if y > y_max:
            y_max = y
    dy = y_max - y_min
    logger.info("x range: {} -> {}".format(x_min, x_max))
    logger.info("y range: {} -> {}".format(y_min, y_max))

    test_area = dx * dy
    seed = int(time.time())
    generator = MyRandomGenerator(seed)

    logger.info("test area: {}".format(test_area))

    runs = [10, 100, 1000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000]
    results = []
    calculation_times = []
    for points in runs:
        start = time.time()
        counter = 0
        integral = 0
        for i in range(points):
            x = x_min + dx * generator.uni01()
            y = y_min + dy * generator.uni01()
            f_x = f(x)
            if math.fabs(y) < math.fabs(f_x):
                if y < 0 and f_x < 0:
                    counter -= 1
                elif y > 0 and f_x > 0:
                    counter += 1

        integral = test_area * float(counter) / points
        if y_min > 0:
            integral += y_min * dx
        elif y_max < 0:
            integral += y_max * dx
        end = time.time()
        logger.info(
            "Deviation ({} shots): {}%, calculation time: {}s".format(
                points,
                round((1 - integral / analytic_result) * 100, 5),
                round(end - start, 4),
            )
        )
        results.append(math.fabs(1 - integral / analytic_result))
        calculation_times.append(end - start)

    fig, ax1 = plt.subplots()
    ax1.axhline(0, color="black")
    ax1.set_xlabel("shots")
    ax1.set_ylabel("deviation", color="tab:red")
    ax1.loglog(runs, results, marker="o", color="tab:red")

    ax2 = ax1.twinx()
    ax2.set_ylabel("calculation time (s)")
    ax2.plot(runs, calculation_times, marker="x")

    fig.savefig("out.png")


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="Monte-Carlo Integral (version {version}) by {author}".format(
            version=__version__, author=__author__
        ),
    )

    args = parser.parse_args()
    main(args)
